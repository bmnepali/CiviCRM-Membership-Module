(function(angular, $, _) {
  // "datepicker" is a basic skeletal directive.
  angular.module('memberships').directive('datepicker', function() {
    return {
      restrict: "A",
      require: "ngModel",
      link: function (scope, elm, attrs, action) {
        var datepicker = function (dateText) {
          scope.$apply(function () {
            action.$setViewValue(dateText);
          });
        };
        var options = {
          dateFormat: "yy-mm-dd",
          onChange: function (dateText) {
            datepicker(dateText);
          }
        };
        elm.datepicker(options);
      }
    };
  });
})(angular, CRM.$, CRM._);
