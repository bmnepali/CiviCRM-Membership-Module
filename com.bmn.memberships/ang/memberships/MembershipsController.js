(function(angular, $, _) {

  angular.module('memberships').config(function($routeProvider) {
      $routeProvider.when('/memberships', {
        controller: 'MembershipsController',
        templateUrl: '~/memberships/MembershipsController.html',

        // If you need to look up data when opening the page, list it out
        // under "resolve".
        resolve: {
          memberships: function() {
            return CRM.api3('Membership', 'get', {
              "sequential": 1
            }).done(function(result) {
              return result;
            });
          }
        }
      });
    }
  );

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  //   myContact -- The current contact, defined above in config().
  angular.module('memberships').controller('MembershipsController', function($scope, crmApi, crmStatus, crmUiHelp, memberships) {
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('memberships');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/memberships/MembershipsController'}); // See: templates/CRM/memberships/MembershipsController.hlp

    // We have myContact available in JS. We also want to reference it in HTML.
    $scope.memberships = memberships;
    $scope.error_message = 0;

    $scope.filters = {
      'join_date': '',
      'start_date': '',
      'end_date': '',
      'search_value': {"sequential": 1}
    }

    $scope.$watch('filters.join_date', function (newValue, oldValue, scope) {
      if(newValue){
        $scope.filters.sdisable = $scope.filters.edisable = true;
        $scope.search_value = {'join_date':{">": newValue}};
      }else{
        $scope.filters.sdisable = $scope.filters.edisable = false;
        $scope.search_value = {"sequential": 1};
      }
    }, true);

    $scope.$watch('filters.start_date', function (newValue, oldValue, scope) {
      if(newValue){
        $scope.filters.jdisable = $scope.filters.edisable = true;
        $scope.search_value = {'start_date':newValue};
      }else{
        $scope.filters.jdisable = $scope.filters.edisable = false;
        $scope.search_value = {"sequential": 1};
      }
    }, true);

    $scope.$watch('filters.end_date', function (newValue, oldValue, scope) {
      if(newValue){
        $scope.filters.sdisable = $scope.filters.jdisable = true;
        $scope.search_value = {'end_date':newValue};
      }else{
        $scope.filters.sdisable = $scope.filters.jdisable = false;
        $scope.search_value = {"sequential": 1};
      }
    }, true);

    $scope.search = function search() {
      return crmStatus(
        // Status messages. For defaults, just use "{}"
        {start: ts('Searching the results ...'), success: ts('Results Displayed!')},
        // The save action. Note that crmApi() returns a promise.
        CRM.api3('Membership', 'get', $scope.search_value).done(function(result) {
          console.log('hh',result.values)
          if(result.values != ''){
            $scope.memberships = result;
            $scope.error_message = 0;
          }else{
            $scope.error_message = 1;
            $scope.memberships = [];
          }
          $scope.$apply();
        })
      );
    };
  });

})(angular, CRM.$, CRM._);
