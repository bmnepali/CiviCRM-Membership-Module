# CiviCRM Membership Filtration Module
=========================================

This is the simple CiviCRM Extension written based on the Angularjs and CIVIX
Commandline tool. It also adds a Menu in navigation in CiviCRM Dashboard as "Search Memberships"

This Module is created for learning purpose, use this module in your own risk.
This will be a great option for newbies in CiviCRM Module Development. Enjoy!

## Requirements:

1. Drupal 7.50
2. CiviCRM 4.7


## Installation

Simply download or use the CiviCRM Module Installation guide.


## Tests:
The tests of the Module are created using Karma and Jasmine. You can simply run
the for the module doing following commands in terminal.

### First Time

1. Install Nodejs
2. Perform the following commands
```
npm Install
npm install -g gulp
```

### Running tests
To run the tests do the following commands
```
npm test
```
And you are all set to go! Enjoy Learning!


Buddha Man Nepali
