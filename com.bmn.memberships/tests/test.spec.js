(function () {
	'use strict';

	describe('1. CiviCRM Membership Extension Tests', function () {
    // Initialize required variables
    var ctrl, scope, CRM, api;

    beforeEach(module('memberships'));

    beforeEach(inject(function($controller, $rootScope){
      scope = $rootScope.$new();
      var crmApi = {};
      var crmStatus = {};
      var crmUiHelp = function(){}
      var memberships = {};
      ctrl = $controller('MembershipsController', {
        $scope: scope, crmApi, crmStatus, crmUiHelp, memberships
      });
    }));

    describe("1.1 Basic Controller Tests", function(){
      it("Memberships Conrtoller should be available", function () {
        expect(ctrl).toBeTruthy()
      });

      it("Memberships Conrtoller should have mambership initialiazed", function () {
        expect(scope.memberships).toEqual({})
      });

      it("Memberships Conrtoller should have Filter Fields", function () {
        expect(scope.filters.join_date).toBeDefined()
        expect(scope.filters.start_date).toBeDefined()
        expect(scope.filters.end_date).toBeDefined()
      });

      it("Memberships Conrtoller should have default search value", function () {
        expect(scope.filters.search_value).toEqual({"sequential": 1})
      });
    });

    describe("1.2 Controller\'s Functionallity  using Spying", function(){
      it("Should call search method opon calling", function () {
        spyOn(scope, 'search');
        scope.search();
        expect(scope.search).toHaveBeenCalled();
      });
    });
	});
}());
